
#!/bin/bash

i="0"

while [ $i -lt 25000 ]
do
    i=$[$i+1]

    echo 'running jenkins cli test commands'

    sudo ./appkido -c chisels/txn_model.lua ussclsetomprg01 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomprg01.scap0
    sudo ./appkido -c chisels/txn_model.lua ussclsetomprg01 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomprg01.scap1
    sudo ./appkido -c chisels/txn_model.lua ussclsetomprg02 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomprg02.scap0
    sudo ./appkido -c chisels/txn_model.lua ussclsetomprg02 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomprg02.scap1

    sudo ./appkido -c chisels/txn_model.lua ussclsetomcrg01 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomcrg01.scap0
    sudo ./appkido -c chisels/txn_model.lua ussclsetomcrg01 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomcrg01.scap1
    sudo ./appkido -c chisels/txn_model.lua ussclsetomcrg02 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomcrg02.scap0
    sudo ./appkido -c chisels/txn_model.lua ussclsetomcrg02 -r ~/work/scaps/autodesk_scaps/Appkido.scaps.ussclsetomcrg02.scap1

done

