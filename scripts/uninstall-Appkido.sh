#!/bin/sh

killall -9 start-Appkido.sh
echo "Appkido: stopped healthcheck"

sleep 1

# uninstall driver
do_sensor_uninstall() {
    pkill -9 appkido
    sleep 1
    if [ -e "/dev/appkido0" ]; then
        rmmod appkido_sensor
    fi
    sleep 1
}
do_sensor_uninstall
echo "Appkido stopped and un-installed"
