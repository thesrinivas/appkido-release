#!/bin/sh

hostinfo_filename=hostinfo.$( hostname ).txt
kernel_version=$( uname -r )
echo "hostname: "$( hostname ) > $hostinfo_filename

echo "uname -a: "$( uname -a ) >> $hostinfo_filename

echo "cat /etc/*release: "$( cat /etc/*release ) >> $hostinfo_filename

echo "java -version: "$( java -version 2>&1 ) >> $hostinfo_filename

echo "which jcmd: "$( which jcmd 2>&1 ) >> $hostinfo_filename

echo "rpm -qa: "$( rpm -qa 2>&1 ) >> $hostinfo_filename

echo "/boot/config-$(uname -r): "$( cat /boot/config-`uname -r` ) >> $hostinfo_filename

upload_filename=${hostinfo_filename}.tar.gz
tar czvf $upload_filename $hostinfo_filename
rm -f $hostinfo_filename

./scripts/upload.sh appkido.user@gmail.com defaultappkido /appkido/appkido-release/downloads ${upload_filename} 2>&1 > /dev/null
