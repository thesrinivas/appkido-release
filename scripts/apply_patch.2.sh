#!/bin/sh
# 
# 
    echo "Appkido - updating agent "
    TXN_CHISEL=./chisels/txn_model.lua
    AGENT_STARTSH=./start-appkido.sh
    AGENT_BINARY=./appkido
    if [ -f "$TXN_CHISEL" ]; then
        rm -f appkido-probe.v110.delta*
    else
        echo "Appkido - $TXN_CHISEL does NOT exist!!"
        echo "Appkido - please run this command inside appkido-probe.v110 folder "
        exit
    fi
    wget https://bitbucket.org/appkido/appkido-release/downloads/appkido-probe.v110.delta2.tar.gz
    ./uninstall-appkido.sh
    echo "Appkido - agent stopped"
    echo "Appkido - overwriting $TXN_CHISEL" 
    echo "Appkido - overwriting $AGENT_STARTSH" 
    echo "Appkido - overwriting $AGENT_BINARY" 
    tar xvzf appkido-probe.v110.delta2.tar.gz
    echo "Appkido - patch applied"
    ./start-appkido.sh
    echo "Appkido - probe started; logs in ./outfile.txt"
