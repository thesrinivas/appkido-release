#!/bin/sh

# 
# 
    echo "Appkido - updating agent lua file"
    TXN_CHISEL=./chisels/txn_model.lua
    if [ -f "$TXN_CHISEL" ]; then
        rm -f appkido-probe.v110.delta1.tar.gz
    else
        echo "Appkido - $TXN_CHISEL does NOT exist!!"
        echo "Appkido - please run this command inside appkido-probe.v110 folder "
        exit
    fi
    wget https://bitbucket.org/appkido/appkido-release/downloads/appkido-probe.v110.delta1.tar.gz
    echo "Appkido - overwriting $TXN_CHISEL" 
    tar xvzf appkido-probe.v110.delta1.tar.gz
    echo "Appkido - patch applied"
    ./uninstall-appkido.sh
    echo "Appkido - probe stopped"
    ./start-appkido.sh
    echo "Appkido - probe started"
