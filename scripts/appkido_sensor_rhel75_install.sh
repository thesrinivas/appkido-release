#!/bin/sh
# 
    echo "Appkido - installing sensor"
    VERSION=v100
    SENSOR_DIR=appkido-sensor.${VERSION}
    SENSOR_DIR_BKUP=appkido-sensor.${VERSION}.bak
    PKG_NAME=appkido-sensor.rhel75_${VERSION}.tar.gz
    if [ -e "$SENSOR_DIR" ]; then
        echo "Found $SENSOR_DIR; if a backup is not present make a backup"
        if [ -e "$SENSOR_DIR_BKUP" ]; then
            echo "Found $SENSOR_DIR; backup to $SENSOR_DIR_BKUP"
            cp -r $SENSOR_DIR $SENSOR_DIR_BKUP
        fi
    else
        echo "Appkido - installing Appkido sensor $SENSOR_DIR "
    fi
    rm -rf $SENSOR_DIR
    rm -f ${PKG_NAME}
    wget https://bitbucket.org/appkido/appkido-release/downloads/${PKG_NAME}
    echo "Appkido - downloaded $PKG_NAME" 
    tar xvzf ${PKG_NAME}
    echo "Appkido - untar sensor package done"
    cd ${SENSOR_DIR} && ./uninstall-Appkido.sh 
    sudo nohup ./start-Appkido.sh > healthcheck.outfile.txt 2>&1 &
    echo "Appkido - sensor $VERSION started"
