#!/bin/bash

i="0"

while [ $i -lt 25000 ]
do
    i=$[$i+1]

    echo "---------" >> monitor_agent.out
    date >> monitor_agent.out
    echo "counter" $i  >> monitor_agent.out
    echo "counter" $i

    #sudo /home/sri/go/bin/cpustat -n 5
    top -n 1 -b  >> monitor_agent.out

    sleep 10 

done

