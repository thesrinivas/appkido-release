#!/bin/sh
# 
    echo "Appkido - installing sensor"
    VERSION=v100
    SENSOR_DIR=appkido-sensor.$VERSION
    if [ -e "$SENSOR_DIR" ]; then
        echo "Found $SENSOR_DIR overwriting"
        rm -rf $SENSOR_DIR
    else
        echo "Appkido - installing Appkido sensor $SENSOR_DIR "
    fi
    HOST_KERNEL=$( uname -r )
    SENSOR_NAME="appkido-sensor."$HOST_KERNEL"_"$VERSION".tar.gz"
    SENSOR_DIR="appkido-sensor."$VERSION
    rm -f ${SENSOR_NAME}
    wget -q https://bitbucket.org/appkido/appkido-release/downloads/${SENSOR_NAME}
    echo "Appkido - downloaded $SENSOR_NAME" 
    tar xvzf ${SENSOR_NAME}
    echo "Appkido - untar sensor package done"
    cd ${SENSOR_DIR} && ./uninstall-appkido.sh 
    sudo nohup ./start-appkido.sh > healthcheck.outfile.txt 2>&1 &
    echo "Appkido - sensor started"
