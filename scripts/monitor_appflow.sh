#!/bin/bash

i="0"

while [ $i -lt 25000 ]
do
    i=$[$i+1]

    echo "date"
    date
    echo "counter"
    echo $i

    # total record count in appflow"
    echo "total count of flowrecs in appflow"
    mongo -quiet "appflow" --eval "db.appflow.count()"
    #echo "total with java pName rec count in appflow"
    #mongo -quiet "appflow" --eval "db.appflow.find( {fline: {\$elemMatch: {pName: \"java\"}}}).count()"
    #echo "total with java tName rec count in appflow"
    #mongo -quiet "appflow" --eval "db.appflow.find( {fline: {\$elemMatch: {tName: \"java\"}}}).count()"
    echo "total count of java flowrecs "
    mongo -quiet "appflow" --eval "db.appflow.find( {fline: {\$elemMatch: {\$or: [{tName: \"java\"}, {pName:\"java\"}]}}}).count()"

    # until we fix the bug in the agent use the following
    # check if java process or thread sent out this alert; this still does not mean RCE attack (parent thread also needs to be checked)
    echo "total count of java flowrecs with execve"
    mongo -quiet "appflow" --eval "db.appflow.find( {\$and:  [{evType :{\$regex: 'ALERT'}}, {fline: {\$elemMatch: {tName: \"java\"}}}]}).count()"

    echo "total count of flowrecs from ussclsetomcrg01"
    mongo -quiet "appflow" --eval "db.appflow.find({\"agentId\":\"ussclsetomcrg01\"}).count()"
    echo "total count of flowrecs from ussclsetomcrg02"
    mongo -quiet "appflow" --eval "db.appflow.find({\"agentId\":\"ussclsetomcrg02\"}).count()"
    echo "total count of flowrecs from ussclsetomprg01"
    mongo -quiet "appflow" --eval "db.appflow.find({\"agentId\":\"ussclsetomprg01\"}).count()"
    echo "total count of flowrecs from ussclsetomprg02"
    mongo -quiet "appflow" --eval "db.appflow.find({\"agentId\":\"ussclsetomprg02\"}).count()"

    echo 'sleeping for 30 secs'
    sleep 30

done

