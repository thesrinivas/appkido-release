#!/bin/sh

# check if  appkido sensor is installed
is_driver_installed() {
    status=0
    if [ -e "/dev/appkido0" ]; then
        status=1
    fi
    echo $status
}

# check if appkido sensor is running
is_appkido_running() {
    status=0
    pid="$(pgrep appkido)"
    if [ -z "$pid" ]
    then
        :
        #echo "appkido not running Pid empty"
    else
        #echo "appkido Pid $pid "
        status=1
    fi
    echo $status
}

# by convention 11 means do upgrade
# 12 means upgrade done
# 10 or anything else dont care
status_upgrade=11
status_upgrade_done=12
sensor_updateconf_url=http://hxe.kontainerdog.com:3000/sensorconf/updatesensorconf/
sensor_getconf_url=http://hxe.kontainerdog.com:3000/sensorconf/getsensorconf/
agentId=( hostname )

# set appkido sensor upgrade done
set_upgrade_done() {
    curl -s -X POST -H "Content-Type: application/json" --data '{"agentId":"'${agentId}'","flags":'${status_upgrade_done}'}' http://hxe.kontainerdog.com:3000/sensorconf/updatesensorconf
}

# check if appkido sensor upgrade required
is_upgrade_required() {
    upgrade=0
    #for now agentId is the same as hostname
    upgrade_flag=`curl -s -X GET -H "Content-Type: application/json" --data '{"hostname":"'"${agentId}"'"}' ${sensor_getconf_url} | ./bin/jq '.[0].flags'`

    if [[ $upgrade_flag == $status_upgrade ]]; then
        upgrade=1
    fi
    echo $upgrade
}

# uninstall appkido sensor
do_sensor_uninstall() {
    # if driver is being, we cant uninstall
    # first kill any processes using it
    pid="$(pgrep appkido)"
    if [ -z "$pid" ]
    then
        :
        #echo "start-Appkido not running Pid empty"
    else
        #echo "start-Appkido Pid $pid "
         kill -9 $pid
        sleep 1
    fi
    if [ -e "/dev/appkido0" ]; then
        status=1
         rmmod appkido_sensor
    fi
    echo 0
}

# install driver
do_driver_install() {
    if [ -e "/dev/appkido0" ]; then
         rmmod appkido_sensor
    fi
     insmod appkido-sensor.ko behControlEnable=1 behControlJsonFileName="./controls/rce-cves.tomcat.json"
    # insmod appkido-sensor.ko behControlEnable=1 behControlDebug=1 behControlJsonFileName="./controls/rce-cves.tomcat.json"
    # insmod appkido-sensor.ko 
    status=$?

    echo $status
}

# collect logs
do_collect_sensor_restartlogs() {
    # collect sensor and healthcheck logs out
    echo 'Appkido: collect sensor and healthcheck logs out'
    sensorlogs_filename="sensor_restartlogs_"$( hostname )".tar.gz"
    tar --ignore-failed-read -cvzf ${sensorlogs_filename} $( ls sensor.outfile.txt healthcheck.outfile.txt 2>/dev/null )
    ./scripts/upload.sh appkido.user@gmail.com defaultappkido /appkido/appkido-release/downloads ${sensorlogs_filename} 2>&1 > /dev/null
    rm -f ${sensorlogs_filename}
}

# collect logs
do_collect_sensor_startuplogs() {
    # collect sensor and healthcheck logs out
    echo 'Appkido: collect sensor and healthcheck logs out'
    sensorlogs_filename="sensor_startuplogs_"$( hostname )".tar.gz"
    tar --ignore-failed-read -cvzf ${sensorlogs_filename} $( ls sensor.outfile.txt healthcheck.outfile.txt 2>/dev/null )
    ./scripts/upload.sh appkido.user@gmail.com defaultappkido /appkido/appkido-release/downloads ${sensorlogs_filename} 2>&1 > /dev/null
    rm -f ${sensorlogs_filename}
}

# start sensor
do_sensor_start() {
    status=0
    
    status=( do_sensor_uninstall )

    echo "Appkido installing driver "
    driver_installed=$( do_driver_install )

    if [ $driver_installed -gt 0 ]; then
        :
        echo "Appkido - failed to install driver; date "$( date -u )
        status=1
    else
         rm -f sensor.outfile.txt
         nohup ./appkido -c txn_model.lua > sensor.outfile.txt 2>&1 &
        echo "Appkido - started sensor; "$( date -u )

        echo "Appkido - set upgrade done; "$( date -u )
        set_upgrade_done
        status=0
    fi

    echo $status
}

    init_state=0
    # to avoid a restart loop we keep count and stop automatic restarts after the max
    restart_count=0
    max_restart_count=50
    # while [ $(date +%H:%M) != "04:00" ] || [ $start -eq 1 ]
    while [ 1 ]
    do
        # upload hostinfo only first time we install sensor
        if [ $init_state -eq 0 ]; then
                echo "Appkido - gather hostinfo; "$( date -u )
                ./scripts/hostinfo.sh

                # start sensor
                echo "Appkido - start sensor; "$( date -u )

                do_sensor_start
                echo "Appkido - started sensor; "$( date -u )
                init_state=1
                restart_count=1

                do_collect_sensor_startuplogs
        else
            # check if we're running ok
            appkido_running=$( is_appkido_running )

            if [ $appkido_running -gt 0 ]; then
                echo "Appkido - running; "$( date -u )
            else
                echo "Appkido - NOT running; "$( date -u )
                echo "Appkido - restart_count: "$restart_count
            fi

            # check if upgrade is required
            upgrade_required=$( is_upgrade_required )
            if [ $upgrade_required -gt 0 ]; then
                echo "Appkido sensor upgrade_required: "$upgrade_required
            else
                echo "Appkido sensor upgrade NOT required"
            fi

            # if upgrade is required we stop and reinstall and start
            if [ $upgrade_required -eq 1 ]; then
                # upgrade
                # stop appkido process, uninstall driver,
                # download and install of new package and exit from here
                echo "Appkido upgrade start; uninstall sensor"
                sensor_uninstalled=$( do_sensor_uninstall )

                do_collect_sensor_restartlogs

                echo "start install"
                # start upgrade
                cd .. && wget -O - https://bitbucket.org/appkido/appkido-release/downloads/appkido_sensor_rhel75_install.sh | sh
                exit 0
            else
                upgrade_filename="upgrade_sensor_"$( hostname )
                if [ $appkido_running -gt 0 ]; then
                    echo "Appkido running & upgrade NOT required "$( date -u )
                else
                    echo "Appkido: NOT running "$( date -u )
                    sensor_uninstalled=$( do_sensor_uninstall )

                    do_collect_sensor_restartlogs
                    echo "Appkido: start sensor; restart_count "$restart_count
                    ((restart_count=restart_count+1))

                    echo "Appkido: restart count "$restart_count
                    if [[ $restart_count>$max_restart_count ]]; then
                        echo "Appkido: max restart count reached! "$restart_count
                        exit 0
                    fi
                    echo "Appkido: starting sensor"
                    do_sensor_start
                fi
            fi
        fi
        # check once a min
        echo "Appkido healthcheck sleep "
        sleep 60
    done
